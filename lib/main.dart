import 'package:bloc_test/bloc/todo/todo_bloc.dart';
import 'package:bloc_test/bloc/todo/todo_state.dart';
import 'package:bloc_test/page/home_page.dart';
import 'package:flutter/material.dart';


import 'package:flutter_bloc/flutter_bloc.dart';void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => TodoBloc(UninitializedTodoState()),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home:HomePage(),
      ),
    );
  }
}