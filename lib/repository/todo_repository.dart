import 'package:bloc_test/prototype/todo_item.dart';

class TodoRepository {
  List todoList = [];

  static final TodoRepository _todoRepository = TodoRepository._internal();

  factory TodoRepository() {
    return _todoRepository;
  }

  TodoRepository._internal();
}