class TodoState {}

class UninitializedTodoState extends TodoState {

}

class HasTodoState extends TodoState {
  final List todoList;

  HasTodoState(this.todoList);
}