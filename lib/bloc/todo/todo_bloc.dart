import 'package:bloc/bloc.dart';
import 'package:bloc_test/bloc/todo/todo_event.dart';
import 'package:bloc_test/bloc/todo/todo_state.dart';
import 'package:bloc_test/prototype/todo_item.dart';
import 'package:bloc_test/repository/todo_repository.dart';

class TodoBloc extends Bloc<TodoEvent, TodoState> {
  TodoBloc(TodoState initialState) : super(initialState);

  @override
  Stream<TodoState> mapEventToState(TodoEvent event) async* {

    TodoRepository todoRepository = TodoRepository();

    if (event is AddTodoEvent){
      todoRepository.todoList.add(new TodoItem(title: event.title, content: event.content));
      yield HasTodoState(todoRepository.todoList);
    }else if (event is RemoveTodoEvent){
      todoRepository.todoList.removeAt(event.index);
      yield HasTodoState(todoRepository.todoList);
    }else if (event is ResetTodoEvent){
      yield UninitializedTodoState();
    }
  }

}