class TodoEvent {}

class AddTodoEvent extends TodoEvent {
 final String title;
 final String content;

 AddTodoEvent({this.title, this.content});
}

class RemoveTodoEvent extends TodoEvent {
 final int index;

 RemoveTodoEvent({this.index});
}

class ResetTodoEvent extends TodoEvent {

}