import 'package:bloc_test/bloc/todo/todo_bloc.dart';
import 'package:bloc_test/bloc/todo/todo_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TodoItemWidget extends StatelessWidget {

  String title;
  String content;
  int index;

  TodoItemWidget({String title, String content, int index}){
    this.title = title;
    this.content = content;
    this.index = index;
}
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      decoration: BoxDecoration(
        color: Colors.white,
          boxShadow: [
          BoxShadow(
          color: Colors.grey.withOpacity(0.2),
          spreadRadius: 1,
          blurRadius: 4,
          offset: Offset(0, 3), // changes position of shadow
        ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Spacer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(this.title, style: TextStyle(fontWeight: FontWeight.w700, fontSize: 17.0)),
              Text(this.content),

            ],
          ),
          Spacer(),
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () {
              BlocProvider.of<TodoBloc>(context).add(RemoveTodoEvent(index:this.index));
            },
          ),
        ],
    ),
    );
  }
}
