import 'package:bloc_test/bloc/todo/todo_bloc.dart';
import 'package:bloc_test/bloc/todo/todo_event.dart';
import 'package:bloc_test/bloc/todo/todo_state.dart';
import 'package:bloc_test/page/widgets/todo_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_beautiful_popup/main.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final _formKey = GlobalKey<FormState>();
    String _title = "";
    String _content = "";

    final popup = BeautifulPopup(
      context: context,
      template: TemplateTerm,
    );

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage('https://wallpapercave.com/wp/wp3844077.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: BlocBuilder<TodoBloc, TodoState>(
                builder: (context, state){
                  if ( state is HasTodoState){
                    return new ListView.builder
                      (
                        itemCount:  state.todoList.length,
                        itemBuilder: (BuildContext ctxt, int i) {
                          return Padding(
                            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                            child: new TodoItemWidget(
                                title: state.todoList[i].title,
                                content: state.todoList[i].content,
                                index: i
                            ),
                          );
                        }
                    );
                  }
               return Column(
                 mainAxisAlignment: MainAxisAlignment.start,
                 children: <Widget>[
                   Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: <Widget>[
                       Container(height: 300.0,),
                     Text("No Todo here :(", style: TextStyle(fontWeight: FontWeight.w700, color: Color.fromARGB(255, 251, 139, 60), fontSize: 40.0),)
                   ],)
                 ],
               );
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 20.0),
              child: ClipOval(
                child: Material(
                  color: Color.fromARGB(255, 250, 171, 63), // button color
                  child: InkWell(
                    splashColor: Color.fromARGB(255, 251, 139, 60), // inkwell color
                    child: SizedBox(width: 56, height: 56, child: Icon(Icons.add)),
                    onTap: () {
                      popup.show(
                        title: 'Add a new Item',
                        content: Form(
                            key: _formKey,
                            child: Column(
                                children: <Widget>[
                                  Text("Title", style: TextStyle(
                                    color: Color.fromARGB(255, 251, 139, 60),
                                    fontWeight: FontWeight.w700
                                  )),
                                  TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      _title = value;
                                      return null;
                                    },
                                  ),
                                  Container(height: 30.0,),
                                  Text("Content", style: TextStyle(
                                      color: Color.fromARGB(255, 251, 139, 60),
                                      fontWeight: FontWeight.w700
                                  )),
                                  TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      _content = value;
                                      return null;
                                    },
                                  )
                                ]
                            )
                        ),
                        actions: [
                          popup.button(
                            label: 'Add',
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                BlocProvider.of<TodoBloc>(context).add(AddTodoEvent(title:_title, content: _content));
                                Navigator.of(context).pop();
                              }
                            }
                          ),
                        ],
                        // bool barrierDismissible = false,
                        // Widget close,
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
