class TodoItem {
  String title;
  String content;

  TodoItem({String title, String content}){
    this.title = title;
    this.content = content;
  }

}